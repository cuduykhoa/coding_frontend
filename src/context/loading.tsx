import React, { createContext, useCallback, useState } from "react";

import { Loading } from "../components";

interface LoadingContextProps {
  loading: boolean;
  showLoading: () => void;
  hideLoading: () => void;
}

export const LoadingContext = createContext<LoadingContextProps>({
  loading: true,
  showLoading: () => {},
  hideLoading: () => {},
});

export const LoadingContextProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [isLoading, setLoading] = useState(false);
  const hideLoading = useCallback(() => {
    setLoading(false);
  }, []);
  const showLoading = useCallback(() => {
    setLoading(true);
  }, []);

  return (
    <LoadingContext.Provider
      value={{
        loading: isLoading,
        showLoading,
        hideLoading,
      }}
    >
      {isLoading && <Loading />}
      {children}
    </LoadingContext.Provider>
  );
};
