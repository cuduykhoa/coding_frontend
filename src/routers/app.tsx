import React, { Suspense } from "react";
import { Router } from "react-router-dom";

import Layout from "../layouts";
import useHistory from "../hooks/useHistory";

export default function AppRouter() {
  const history = useHistory();

  return (
    <Router history={history}>
      <Suspense fallback={<div>Loading...</div>}>
        <Layout />
      </Suspense>
    </Router>
  );
}
