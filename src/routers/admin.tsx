import { lazy } from "react";

const routers = [
  {
    path: "/",
    component: lazy(() => import("../pages/Dashboard")),
  },
  {
    path: "/dashboard",
    component: lazy(() => import("../pages/Dashboard")),
  },
];

export default routers;
