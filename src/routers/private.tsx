import { lazy } from "react";

const routers = [
  {
    path: "/",
    component: lazy(() => import("../pages/Blog")),
  },
  {
    path: "/blogs",
    component: lazy(() => import("../pages/Blog")),
  },
];

export default routers;
