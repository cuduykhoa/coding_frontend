import { lazy } from "react";

const routers = [
  {
    path: "/",
    component: lazy(() => import("../pages/SignIn")),
  },
  {
    path: "/signin",
    component: lazy(() => import("../pages/SignIn")),
  },
  {
    path: "/signup",
    component: lazy(() => import("../pages/SignUp")),
  },
];

export default routers;
