import React from "react";
import { Switch, Route } from "react-router-dom";

import NotFound from "../../pages/NotFound";
import routes from "../../routers/public";

const PublicLayout = () => {
  return (
    <div>
      <Switch>
        {routes.map((route) => {
          return <Route key={route.path} exact {...route} />;
        })}
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </div>
  );
};

export default PublicLayout;
