import React from "react";

import useAuthState from "../hooks/useAuthState";
import PublicLayout from "./PublicLayout";
import PrivateLayout from "./PrivateLayout";
import AdminLayout from "./AdminLayout";

const Layout = () => {
  const state = useAuthState();

  switch (state) {
    case "user":
      return <PrivateLayout />;
    case "admin":
      return <AdminLayout />;
    default:
      return <PublicLayout />;
  }
};

export default Layout;
