import React, { useCallback } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { Header } from "../../components";
import routes from "../../routers/admin";
import { signOut } from "../../services/firebase";

import "draft-js/dist/Draft.css";

const AdminLayout = () => {
  const handleSignOut = useCallback(async () => {
    await signOut();
  }, []);

  return (
    <div>
      <Header onSignOut={handleSignOut} />
      <Switch>
        {routes.map((route) => {
          return <Route key={route.path} exact {...route} />;
        })}
        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </div>
  );
};

export default AdminLayout;
