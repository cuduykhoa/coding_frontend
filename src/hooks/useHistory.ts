import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const useHistory = () => history

export default useHistory
