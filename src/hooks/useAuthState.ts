import { useCallback, useEffect, useState } from 'react'

import { onAuthStateChanged } from '../services/firebase'
import useLoading from '../hooks/useLoading'

const useAuthState = () : string => {
    const { hideLoading } = useLoading()
    const [authState, setAuthState] = useState('')

    const handleAuthStateChanged = useCallback((state) => {
        setAuthState(state)
        hideLoading()
    }, [hideLoading])

    useEffect(() => {
        onAuthStateChanged({ onChange: handleAuthStateChanged })
    }, [handleAuthStateChanged])

    return authState
};

export default useAuthState
