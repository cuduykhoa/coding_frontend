import React from "react";

import AppRouter from "./routers/app";
import { LoadingContextProvider } from "./context/loading";

import "./App.css";

function App() {
  return (
    <LoadingContextProvider>
      <AppRouter />
    </LoadingContextProvider>
  );
}

export default App;
