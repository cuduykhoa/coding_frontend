import Axios, { AxiosResponse, AxiosError } from "axios";

import { getToken } from "./firebase";

const onResponseSuccess = (response: AxiosResponse) => response;
const onResponseError = (err: AxiosError) => {
  const status = err?.response?.status;

  if (status === 403 || status === 401) {
    console.error("Unauthenticated");
  }

  return Promise.reject(err?.response || err);
};

Axios.interceptors.response.use(onResponseSuccess, onResponseError);

interface User {
  userId: string;
  email: string;
  name: string;
  birthday: number;
}

interface ImageUrl {
  data: {
    fields: {
      key: string;
      "Content-Type": string;
      Policy: string;
      bucket: string;
      "X-Amz-Algorithm": string;
      "X-Amz-Credential": string;
      "X-Amz-Security-Token": string;
      "X-Amz-Signature": string;
      "x-amz-meta-userid": string;
      "X-Amz-Date": string;
    };
    url: string;
  };
}

export const updateUser = ({ userId, email, name, birthday }: User) => {
  return Axios({
    method: "PUT",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    data: {
      email,
      name,
      birthday,
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/user/${userId}`,
  });
};

export const getBlogs = async () => {
  const result = await Axios({
    method: "GET",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/blogs`,
  });

  return result?.data?.data || [];
};

export const updateBlog = ({
  id,
  title,
  content,
}: {
  id: string;
  title: string;
  content: string;
}) => {
  return Axios({
    method: "PUT",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/blog/${id}`,
    data: {
      title,
      content,
    },
  });
};

export const createBlog = async ({
  title,
  content,
}: {
  title: string;
  content: string;
}) => {
  const result = await Axios({
    method: "POST",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/blogs`,
    data: {
      title,
      content,
    },
  });

  return result?.data?.data?.id;
};

export const getUploadImageUrl = async ({
  blogId,
  fileName,
  contentType,
}: {
  blogId: string;
  fileName: string;
  contentType: string;
}) => {
  const result = await Axios({
    method: "POST",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/blog/attachments`,
    data: {
      blogId,
      fileName,
      contentType,
    },
  });

  return result.data;
};

export const uploadImage = async ({
  image,
  imageInfo,
}: {
  image: File;
  imageInfo: ImageUrl;
}) => {
  const { fields, url } = imageInfo.data;
  const formData = new FormData();

  Object.entries(fields).forEach(([key, value]) => {
    formData.append(key, value);
  }, []);
  formData.append("file", image);

  await Axios.post(url, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export const deleteBlog = ({ blogId }: { blogId: string }) => {
  Axios({
    method: "DELETE",
    headers: {
      Authorization: getToken(),
      "Content-Type": "application/json",
    },
    url: `${process.env.REACT_APP_ENDPOINT_URL}/blog/${blogId}`,
  });
};
