import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";

import { Blog } from "../types/blog";

const config = {
  apiKey: "AIzaSyBSSG6UsvE6X01188qbOQM5lSdFY-7bvXQ",
  authDomain: "coding-c1fbd.firebaseapp.com",
  projectId: "coding-c1fbd",
  storageBucket: "coding-c1fbd.appspot.com",
  messagingSenderId: "486555224434",
  appId: "1:486555224434:web:748ebe2195f00ce47435eb",
};

firebase.initializeApp(config);

let token = "";

interface SignInWithEmailAndPassword {
  email: string;
  password: string;
}

interface SignUpWithEmailAndPassword {
  email: string;
  password: string;
}

interface OnAuthStateChangedProps {
  onChange: (state: string) => void;
}

interface SnapshotBlogProps {
  onChange: (data: Blog[]) => void;
}

const BLOG_COLLECTION = "blog";

export const signInWithEmailAndPassword = ({
  email,
  password,
}: SignInWithEmailAndPassword) => {
  return firebase.auth().signInWithEmailAndPassword(email, password);
};

export const createUserWithEmailAndPassword = ({
  email,
  password,
}: SignUpWithEmailAndPassword) => {
  return firebase.auth().createUserWithEmailAndPassword(email, password);
};

export const onAuthStateChanged = ({ onChange }: OnAuthStateChangedProps) => {
  firebase.auth().onAuthStateChanged(async (user) => {
    if (!user) {
      return onChange("");
    }

    const result = await user.getIdTokenResult();

    token = result.token;

    if (result.claims.admin) {
      return onChange("admin");
    }

    onChange("user");
  });
};

export const getToken = () => token;

export const signOut = () => {
  return firebase.auth().signOut();
};

export const snapshotBlogs = ({ onChange }: SnapshotBlogProps) => {
  const unsubscribe = firebase
    .firestore()
    .collection(BLOG_COLLECTION)
    .where("deletedAt", "==", 0)
    .onSnapshot((snapshot) => {
      const data: Blog[] = [];

      snapshot.forEach((doc) => {
        data.push({ id: doc.id, ...doc.data() } as Blog);
      });

      onChange(data);
    });

  return unsubscribe;
};
