import React, { useCallback, useState } from "react";

import { Button, Input } from "../../components";
import { signInWithEmailAndPassword } from "../../services/firebase";
import useLoading from "../../hooks/useLoading";
import useHistory from "../../hooks/useHistory";

import styles from "./SignIn.module.css";

const SignIn = () => {
  const history = useHistory();
  const { showLoading, hideLoading } = useLoading();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleEmailChange = useCallback((e) => {
    setEmail(e.target.value);
  }, []);

  const handlePasswordChange = useCallback((e) => {
    setPassword(e.target.value);
  }, []);

  const handleSignIn = useCallback(
    async (e) => {
      e.preventDefault();
      try {
        if (!email || !password) {
          setError("Missing email or password");
        } else {
          showLoading();
          await signInWithEmailAndPassword({ email, password });
        }
      } catch (error) {
        const errorMessage = error.message;

        hideLoading();
        setError(errorMessage);
      }
    },
    [email, password, showLoading, hideLoading]
  );

  const handleSignUp = useCallback(() => {
    history.push("/signup");
  }, [history]);

  return (
    <form className={styles.wrapper} onSubmit={handleSignIn}>
      <Input
        label="Email"
        placeholder="Email"
        type="text"
        value={email}
        onChange={handleEmailChange}
      />
      <Input
        label="Password"
        placeholder="Password"
        type="password"
        value={password}
        onChange={handlePasswordChange}
      />
      <div>{error}</div>
      <Button type="submit" title="SignIn" />
      <div className={styles.newAccount} onClick={handleSignUp}>
        Create a new account
      </div>
    </form>
  );
};

export default SignIn;
