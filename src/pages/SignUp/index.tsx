import React, { useState, useCallback, useEffect } from "react";

import { Button, Input, DatePicker } from "../../components";
import { createUserWithEmailAndPassword } from "../../services/firebase";
import useLoading from "../../hooks/useLoading";
import useHistory from "../../hooks/useHistory";
import { updateUser } from "../../services/api";

import styles from "./SignUp.module.css";

const SignUpPage = () => {
  const history = useHistory();
  const { showLoading, hideLoading } = useLoading();
  const [newUser, setNewUser] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [birthday, setBirthday] = useState(new Date());
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleEmailChange = useCallback((e) => {
    setEmail(e.target.value);
  }, []);

  const handlePasswordChange = useCallback((e) => {
    setPassword(e.target.value);
  }, []);

  const handleChangeBirthday = useCallback((e) => {
    setBirthday(e);
  }, []);

  const handleNameChange = useCallback((e) => {
    setName(e.target.value);
  }, []);

  const handleSignIn = useCallback(() => {
    history.push("/signin");
  }, [history]);

  const handleSignUp = useCallback(
    async (e) => {
      e.preventDefault();
      try {
        if (!email || !password) {
          setError("Missing email or password");
        } else {
          showLoading();
          const user = await createUserWithEmailAndPassword({
            email,
            password,
          });

          setNewUser(user?.user?.uid || "");
        }
      } catch (error) {
        const errorMessage = error.message;

        hideLoading();
        setError(errorMessage);
      }
    },
    [email, password, showLoading, hideLoading]
  );

  useEffect(() => {
    return () => {
      if (newUser) {
        updateUser({
          userId: newUser,
          email,
          name,
          birthday: birthday.getTime(),
        });
      }
    };
  }, [newUser, email, name, birthday]);

  return (
    <form className={styles.wrapper} onSubmit={handleSignUp}>
      <Input
        label="Email"
        placeholder="Email"
        type="text"
        value={email}
        onChange={handleEmailChange}
      />
      <Input
        label="Password"
        placeholder="Password"
        type="password"
        value={password}
        onChange={handlePasswordChange}
      />
      <Input
        label="Name"
        placeholder="Name"
        type="text"
        value={name}
        onChange={handleNameChange}
      />
      <DatePicker
        label="Birthday"
        placeholder="Birthday"
        value={birthday}
        onChange={handleChangeBirthday}
      />
      <div>{error}</div>
      <Button type="submit" title="SignUp" />
      <div className={styles.haveAccount} onClick={handleSignIn}>
        Already have account ?
      </div>
    </form>
  );
};

export default SignUpPage;
