import React, { useCallback, useEffect, useState } from "react";

import { snapshotBlogs } from "../../services/firebase";
import { BlogCard } from "../../components";

import styles from "./Blog.module.css";

const BlogPage = () => {
  const [data, setData] = useState([]);

  const handleDataChange = useCallback((data) => {
    setData(data);
  }, []);

  useEffect(() => {
    snapshotBlogs({ onChange: handleDataChange });
  }, [handleDataChange]);

  return (
    <div className={styles.wrapper}>
      {data.map((item) => (
        <BlogCard key={item.id} {...item} />
      ))}
    </div>
  );
};

export default BlogPage;
