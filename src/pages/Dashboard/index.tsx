import React, { useCallback, useEffect, useState } from "react";

import { snapshotBlogs } from "../../services/firebase";
import {
  updateBlog,
  createBlog,
  getUploadImageUrl,
  uploadImage,
  deleteBlog,
} from "../../services/api";
import { Blog } from "../../types/blog";
import { BlogItem, BlogEditor, Modal } from "../../components";
import useModal from "../../hooks/useModel";
import useLoading from "../../hooks/useLoading";

import styles from "./Dashboard.module.css";

const DashboardPage = () => {
  const [data, setData] = useState<Blog[]>([]);
  const { isShowing, toggle } = useModal();
  const { showLoading, hideLoading } = useLoading();

  const handleDataChange = useCallback((data) => {
    setData(data);
  }, []);

  const handleUploadImage = useCallback(async ({ image, blogId }) => {
    const imageInfo = await getUploadImageUrl({
      blogId,
      fileName: image.name,
      contentType: image.type,
    });

    await uploadImage({ image, imageInfo });
  }, []);

  const handleCreate = useCallback(
    ({ title, content, image }) => {
      const createData = async () => {
        const blogId = await createBlog({ title, content });

        hideLoading();

        if (image) {
          await handleUploadImage({ image, blogId });
        }
      };

      showLoading();
      createData();
    },
    [handleUploadImage, showLoading, hideLoading]
  );

  const handleUpdate = useCallback(
    ({ id, title, content, image }) => {
      const updateData = async () => {
        await updateBlog({ id, title, content });

        hideLoading();

        if (image) {
          await handleUploadImage({ image, blogId: id });
        }
      };

      showLoading();
      updateData();
    },
    [handleUploadImage, showLoading, hideLoading]
  );

  const handleDelete = useCallback(
    async ({ id }) => {
      showLoading();
      await deleteBlog({ blogId: id });
      hideLoading();
    },
    [showLoading, hideLoading]
  );

  useEffect(() => {
    snapshotBlogs({ onChange: handleDataChange });
  }, [handleDataChange]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.addButton} onClick={toggle}>
        + Create
      </div>
      <div className={styles.contentWrapper}>
        {data.map((item) => {
          return (
            <BlogItem
              key={item.id}
              {...item}
              onUpdate={handleUpdate}
              onDelete={handleDelete}
            />
          );
        })}
      </div>
      <Modal isShowing={isShowing} hide={toggle}>
        <BlogEditor onCreate={handleCreate} />
      </Modal>
    </div>
  );
};

export default DashboardPage;
