import React, { useCallback, useState } from "react";
import { Editor, EditorState, ContentState } from "draft-js";

import FileUpload from "../FileUpload";
import Image from "../Image";

import styles from "./BlogEditor.module.css";

interface BlogEditorProps {
  id?: string;
  title?: string;
  content?: string;
  image?: string;
  onCreate?: (data: any) => void;
  onUpdate?: (data: any) => void;
  onDelete?: (data: any) => void;
}

const BlogEditor = ({
  id,
  title,
  content,
  image,
  onCreate,
  onUpdate,
  onDelete,
}: BlogEditorProps) => {
  const [error, setError] = useState("");
  const [titleState, setTitleState] = useState(() =>
    title
      ? EditorState.createWithContent(ContentState.createFromText(title))
      : EditorState.createEmpty()
  );
  const [contentState, setContentState] = useState(() =>
    content
      ? EditorState.createWithContent(ContentState.createFromText(content))
      : EditorState.createEmpty()
  );
  const [file, setFile] = useState();

  const handleSave = useCallback(() => {
    const _title = titleState.getCurrentContent().getPlainText();
    const _content = contentState.getCurrentContent().getPlainText();

    if (onCreate) {
      if (_title && _content) {
        onCreate({ title: _title, content: _content, image: file });
      } else {
        setError("Missing title or content");
      }
    }

    if (onUpdate) {
      if (_title || _content || file) {
        onUpdate({
          id,
          title: _title,
          content: _content,
          image: file,
        });
      }
    }
  }, [id, file, titleState, contentState, onCreate, onUpdate]);

  const handleDelete = useCallback(() => {
    onDelete && onDelete({ id });
  }, [id, onDelete]);

  const handleImageChange = useCallback((e) => {
    setFile(e[0]);
  }, []);

  return (
    <div className={styles.wrapper}>
      <div className={styles.actions}>
        <div className={styles.actionButton} onClick={handleSave}>
          Save
        </div>
        {!onCreate && (
          <div className={styles.actionButton} onClick={handleDelete}>
            Delete
          </div>
        )}
      </div>
      {!!error && <h4>{error}</h4>}
      <div className={styles.contentWrapper}>
        <div className={styles.title}>
          <Editor
            placeholder="Title"
            editorState={titleState}
            onChange={setTitleState}
          />
        </div>
        <div className={styles.content}>
          <Editor
            placeholder="Content"
            editorState={contentState}
            onChange={setContentState}
          />
        </div>
        {!file && <Image className={styles.image} src={image} />}
        <FileUpload onChange={handleImageChange} />
      </div>
    </div>
  );
};

export default BlogEditor;
