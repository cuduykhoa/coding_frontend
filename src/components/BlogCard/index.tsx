import React from "react";

import { Blog } from "../../types/blog";
import { Image, Modal } from "../";

import styles from "./BlogCard.module.css";
import useModal from "../../hooks/useModel";

const BlogCard = ({ title, content, image }: Blog) => {
  const { isShowing, toggle } = useModal();

  return (
    <div className={styles.wrapper}>
      <div onClick={toggle}>
        <Image className={styles.image} src={image} />
        <div className={styles.title}>{title}</div>
      </div>
      <Modal isShowing={isShowing} hide={toggle}>
        <h4>{title}</h4>
        <br />
        <Image className={styles.imageModal} src={image} />
        <br />
        <div>{content}</div>
      </Modal>
    </div>
  );
};

export default BlogCard;
