import React, { useState } from "react";
import { useDropzone, DropzoneProps } from "react-dropzone";

import styles from "./FileUpload.module.css";

interface FileUploadProps extends DropzoneProps {
  label?: string;
  onChange: (files: File[]) => void;
}

const FileUpload = ({ label, onChange }: FileUploadProps) => {
  const [files, setFiles] = useState<any[]>([]);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    maxSize: 25 * 1024 * 1024, // 25MB
    accept: ".jpg, .png, .jpeg",
    onDrop: (acceptedFiles) => {
      onChange(acceptedFiles);
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, { preview: URL.createObjectURL(file) })
        )
      );
    },
  });

  return (
    <div className={styles.wrapper}>
      {label && <div className={styles.label}>{label}</div>}
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <div>
            <div>
              <div>Drag and Drop your file or Click to Browse</div>
              <div>Files supported: .jpg, .png, .jpeg (Max. 25 mb)</div>
            </div>
          </div>
        )}
      </div>
      <div className={styles.preview}>
        {!!files.length &&
          files.map((file) => (
            <img
              key={file.name}
              className={styles.previewImage}
              alt="preview"
              src={file.preview}
            />
          ))}
      </div>
    </div>
  );
};

export default FileUpload;
