import React, { useCallback } from "react";

import styles from "./Header.module.css";

interface HeaderProps {
  onSignOut: () => void;
}

const Header = ({ onSignOut }: HeaderProps) => {
  const handleSignOut = useCallback(() => {
    onSignOut();
  }, [onSignOut]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.logOut} onClick={handleSignOut}>
        Logout
      </div>
    </div>
  );
};

export default Header;
