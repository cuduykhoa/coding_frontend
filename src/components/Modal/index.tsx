import React from "react";
import ReactDOM from "react-dom";

import styles from "./Modal.module.css";

interface ModalProps {
  isShowing?: boolean;
  hide: () => void;
  children: React.ReactNode;
}

const Modal = ({ isShowing, hide, children }: ModalProps) =>
  isShowing
    ? ReactDOM.createPortal(
        <div className={styles.wrapper}>
          <div className={styles.overlay} />
          <div className={styles.contentWrapper}>
            <div className={styles.header}>
              <div className={styles.closeButton} onClick={hide}>
                <span aria-hidden="true">&times;</span>
              </div>
            </div>
            <div className={styles.content}>{children}</div>
          </div>
        </div>,
        document.body
      )
    : null;

export default Modal;
