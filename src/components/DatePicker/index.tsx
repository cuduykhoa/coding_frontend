import React from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import styles from "./DatePicker.module.css";

interface DatePickerProps {
  placeholder?: string;
  label: string;
  value: Date;
  onChange: (e: any) => void;
}

const DatePickerComponent = ({
  placeholder,
  label,
  value,
  onChange,
}: DatePickerProps) => {
  return (
    <div className={styles.wrapper}>
      {label && <div className={styles.label}>{label}</div>}
      <DatePicker
        placeholderText={placeholder}
        selected={value}
        onChange={onChange}
      />
    </div>
  );
};

export default DatePickerComponent;
