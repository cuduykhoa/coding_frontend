import React, { useEffect, useState, useRef, useCallback } from "react";

import styles from "./Image.module.css";

interface ImageProps {
  src?: string;
  className?: string;
}

const DEFAULT_IMAGE = `${process.env.PUBLIC_URL}/placeholder.jpeg`;

const Image = ({ src, className }: ImageProps) => {
  const element = useRef<HTMLDivElement>(null);
  const [image, setImage] = useState(DEFAULT_IMAGE);

  const handleIntersect = useCallback(
    (e) => {
      const entry = e[0];

      if (entry.isIntersecting) {
        if (src) {
          const img = new window.Image();

          img.onload = () => {
            setImage(src);
          };
          img.src = src;
        }
      }
    },
    [src]
  );

  useEffect(() => {
    const intersectionObserver = new IntersectionObserver(handleIntersect, {
      threshold: 0.1,
    });

    if (element.current) {
      intersectionObserver.observe(element.current);
    }

    return () => {
      if (intersectionObserver) {
        intersectionObserver.disconnect();
      }
    };
  }, [handleIntersect]);

  return (
    <div
      ref={element}
      className={`${className} ${styles.wrapper}`}
      style={{ backgroundImage: `url("${image}")` }}
    ></div>
  );
};

export default Image;
