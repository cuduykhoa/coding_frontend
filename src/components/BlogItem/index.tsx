import React from "react";

import { Blog } from "../../types/blog";
import { Image, Modal } from "../";
import useModal from "../../hooks/useModel";
import BlogEditor from "../BlogEditor";

import styles from "./BlogItem.module.css";

interface BlogItemProps extends Blog {
  onUpdate?: (data: any) => void;
  onDelete?: (data: any) => void;
}

const BlogItem = ({
  id,
  title,
  content,
  image,
  onUpdate,
  onDelete,
}: BlogItemProps) => {
  const { isShowing, toggle } = useModal();

  return (
    <div className={styles.wrapper}>
      <div onClick={toggle}>
        <Image className={styles.image} src={image} />
        <div className={styles.title}>{title}</div>
      </div>
      <Modal isShowing={isShowing} hide={toggle}>
        <BlogEditor
          id={id}
          title={title}
          content={content}
          image={image}
          onUpdate={onUpdate}
          onDelete={onDelete}
        />
      </Modal>
    </div>
  );
};

export default BlogItem;
